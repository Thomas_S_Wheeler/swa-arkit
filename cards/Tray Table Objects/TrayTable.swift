//
//  TrayTableScene.swift
//  cards
//
//  Created by Thomas Wheeler on 8/7/17.
//  Copyright © 2017 Thomas Wheeler. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

class TrayTable: SCNNode {
    
    var modelName: String = ""
    var fileExtension: String = ""
    var title: String = ""
    var modelLoaded: Bool = false
    
    override init(){
        super.init()
        self.name = "tray table object root node"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(modelName: String, fileExtension: String, title: String) {
        super.init()
        self.name = "tray table object root node"
        self.modelName = modelName
        self.fileExtension = fileExtension
        self.title = title
    }
    
    func loadModel () {
        guard let trayTableScene = SCNScene(named: "\(modelName).\(fileExtension)", inDirectory: "art.scnassets/\(modelName)") else {
            print("fuck")
            return
        }
        
        let wrapperNode = SCNNode()
        
        for child in trayTableScene.rootNode.childNodes {
//            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
            child.movabilityHint = .movable
            wrapperNode.addChildNode(child)
        }
        self.addChildNode(wrapperNode)
        
        modelLoaded = true
    }
    
    func unloadModel() {
        for child in self.childNodes {
            child.removeFromParentNode()
        }
        
        modelLoaded = false
    }

    
}
