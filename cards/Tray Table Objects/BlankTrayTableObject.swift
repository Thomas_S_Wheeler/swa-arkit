//
//  BlankTrayTableObject.swift
//  cards
//
//  Created by Thomas Wheeler on 8/7/17.
//  Copyright © 2017 Thomas Wheeler. All rights reserved.
//

import Foundation

class BlankTrayTableObject: TrayTable {
    
    override init() {
        super.init(modelName: "TrayTable", fileExtension: "scn", title: "TrayTable")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
