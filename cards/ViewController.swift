//
//  ViewController.swift
//  cards
//
//  Created by Thomas Wheeler on 8/3/17.
//  Copyright © 2017 Thomas Wheeler. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        setupScene()
        setupFocusSquare()
//        setupRectangeVisualization()
        setupUIControls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Prevent the screen from being dimmed after a while.
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Start the ARSession.
        restartPlaneDetection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.pause()
    }
    
    // MARK: - UI Controls
    
    var trackingStateLabel: UILabel?
    
    func setupUIControls () {
        
        guard let view = sceneView else { return }
        
        if trackingStateLabel == nil {
            trackingStateLabel = UILabel()
            view.addSubview(trackingStateLabel!)
            
            // label
            trackingStateLabel?.backgroundColor = UIColor.white.withAlphaComponent(0.7)
            trackingStateLabel?.text = "INITIALIZING"
            trackingStateLabel?.textAlignment = .center
            
            // auto layout
            trackingStateLabel?.translatesAutoresizingMaskIntoConstraints = false
            trackingStateLabel?.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
            trackingStateLabel?.heightAnchor.constraint(equalToConstant: 30).isActive = true
            trackingStateLabel?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
            trackingStateLabel?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive = true
        }
    }
    
    // MARK: - ARKit / ARSCNView
    let session = ARSession()
    var sessionConfig: ARSessionConfiguration = ARWorldTrackingSessionConfiguration()
    var use3DOFTracking = false {
        didSet {
            if use3DOFTracking {
                sessionConfig = ARSessionConfiguration()
            }
            sessionConfig.isLightEstimationEnabled = true //UserDefaults.standard.bool(for: .ambientLightEstimation)
            session.run(sessionConfig)
        }
    }
    var use3DOFTrackingFallback = false
    @IBOutlet var sceneView: ARSCNView!
    var screenCenter: CGPoint?
    
    func setupScene() {
        // set up sceneView
        sceneView.delegate = self
        sceneView.session = session
        sceneView.antialiasingMode = .multisampling4X
        sceneView.automaticallyUpdatesLighting = false
        
        sceneView.preferredFramesPerSecond = 60
        sceneView.contentScaleFactor = 1.3
        //sceneView.showsStatistics = true
        
        DispatchQueue.main.async {
            self.screenCenter = self.sceneView.bounds.mid
        }
        
        if let camera = sceneView.pointOfView?.camera {
            camera.wantsHDR = true
            camera.wantsExposureAdaptation = true
            camera.exposureOffset = -1
            camera.minimumExposure = -1
        }
        
        use3DOFTracking = false
    }
    
    func enableEnvironmentMapWithIntensity(_ intensity: CGFloat) {
//        if sceneView.scene.lightingEnvironment.contents == nil {
//            if let environmentMap = UIImage(named: "Models.scnassets/sharedImages/environment_blur.exr") {
//                sceneView.scene.lightingEnvironment.contents = environmentMap
//            }
//        }
        sceneView.scene.lightingEnvironment.intensity = intensity
    }
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
//        refreshFeaturePoints()
        
        DispatchQueue.main.async {
            self.rectangleVisualization?.render()
            self.updateFocusSquare()
//            self.trayTableObject.r
//            self.updateDot()
//            self.updateBox()
//            self.hitTestVisualization?.render()
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            if let planeAnchor = anchor as? ARPlaneAnchor {
                self.addPlane(node: node, anchor: planeAnchor)
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            if let planeAnchor = anchor as? ARPlaneAnchor {
                self.updatePlane(anchor: planeAnchor)
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            if let planeAnchor = anchor as? ARPlaneAnchor {
                self.removePlane(anchor: planeAnchor)
            }
        }
    }
    
    var trackingFallbackTimer: Timer?
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        print(camera.trackingState)
        switch camera.trackingState {
        case .notAvailable:
           print("esc feedback")
           trackingStateLabel?.text = "INITIALIZING: LOOK AROUND"
        case .limited:
            if use3DOFTrackingFallback {
                // After 10 seconds of limited quality, fall back to 3DOF mode.
                trackingFallbackTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { _ in
                    self.use3DOFTracking = true
                    self.trackingFallbackTimer?.invalidate()
                    self.trackingFallbackTimer = nil
                })
            } else {

            }
            trackingStateLabel?.text = "TRACKING STATUS LIMITED"
        case .normal:
            if use3DOFTrackingFallback && trackingFallbackTimer != nil {
                trackingFallbackTimer!.invalidate()
                trackingFallbackTimer = nil
            }
            trackingStateLabel?.text = "TRACKING NORMAL"
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        
        guard let arError = error as? ARError else { return }
        
        let nsError = error as NSError
        var sessionErrorMsg = "\(nsError.localizedDescription) \(nsError.localizedFailureReason ?? "")"
        if let recoveryOptions = nsError.localizedRecoveryOptions {
            for option in recoveryOptions {
                sessionErrorMsg.append("\(option).")
            }
        }
        
        let isRecoverable = (arError.code == .worldTrackingFailed)
        if isRecoverable {
            print("\nYou can try resetting the session or quit the application.")
        } else {
            print("\nThis is an unrecoverable error that requires to quit the application.")
        }
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        print("The session will be reset after the interruption has ended.")
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        session.run(sessionConfig, options: [.resetTracking, .removeExistingAnchors])
        print("RESETTING SESSION")
        trackingStateLabel?.text = "RESETING SESSION"
    }
    
    // MARK: - RectangleVisualization
    
    var rectangleVisualization: RectangleVisualization?
    
    func setupRectangeVisualization() {
        guard let sceneView = self.sceneView else { return }
        rectangleVisualization = RectangleVisualization(sceneView: sceneView)
    }
    
    // MARK: - Planes
    
    var planes = [ARPlaneAnchor: Plane]()
    var dragOnInfinitePlanesEnabled = false
    
    func addPlane(node: SCNNode, anchor: ARPlaneAnchor) {
        
        let pos = SCNVector3.positionFromTransform(anchor.transform)
        print("NEW SURFACE DETECTED AT \(pos.friendlyString())")
        trackingStateLabel?.text = "SURFACE DETECTED"
        
        let plane = Plane(anchor, true)
        
        planes[anchor] = plane
        node.addChildNode(plane)
        
        print("SURFACE DETECTED")
    }
    
    func updatePlane(anchor: ARPlaneAnchor) {
        if let plane = planes[anchor] {
            plane.update(anchor)
        }
    }
    
    func removePlane(anchor: ARPlaneAnchor) {
        if let plane = planes.removeValue(forKey: anchor) {
            plane.removeFromParentNode()
        }
    }
    
    func restartPlaneDetection() {
        
        // configure session
        if let worldSessionConfig = sessionConfig as? ARWorldTrackingSessionConfiguration {
            worldSessionConfig.planeDetection = .horizontal
            session.run(worldSessionConfig, options: [.resetTracking, .removeExistingAnchors])
        }
        
        // reset timer
        if trackingFallbackTimer != nil {
            trackingFallbackTimer!.invalidate()
            trackingFallbackTimer = nil
        }
        
        print("FIND A SURFACE TO PLACE AN OBJECT")
    }
    
    // MARK: - Solitare state
//    var solitareBoardNode: SCNNode?
    var solitaire: Solitaire?
    
    
    // MARK: - Gesture Recognizers
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if solitaire == nil {
//            solitaire = Solitaire()
//            sceneView.scene.rootNode.addChildNode(solitaire!)
//
//            guard let screenCenter = screenCenter else { return }
//            let (worldPos, planeAnchor, _) = worldPositionFromScreenPosition(screenCenter, objectPos: nil)
//            if let worldPos = worldPos {
//                // place box here
//                let position = worldPos + SCNVector3(0, 0.01, 0) // above plane to fix z fighying
//                solitaire?.setPosition(position)
//            }
//        } else {
//            solitaire?.removeFromParentNode()
//            solitaire = nil
//        }
        if (trayTableObject == nil && !isLoadingObject) {
            loadTrayTableObject()
        } else if (trayTableObject != nil && !isLoadingObject) {
            resetTrayTableObject()
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    func worldPositionFromScreenPosition(_ position: CGPoint,
                                         objectPos: SCNVector3?,
                                         infinitePlane: Bool = false) -> (position: SCNVector3?, planeAnchor: ARPlaneAnchor?, hitAPlane: Bool) {
        
        // -------------------------------------------------------------------------------
        // 1. Always do a hit test against exisiting plane anchors first.
        //    (If any such anchors exist & only within their extents.)
        
        let planeHitTestResults = sceneView.hitTest(position, types: .existingPlaneUsingExtent)
        if let result = planeHitTestResults.first {
            
            let planeHitTestPosition = SCNVector3.positionFromTransform(result.worldTransform)
            let planeAnchor = result.anchor
            
            // Return immediately - this is the best possible outcome.
            return (planeHitTestPosition, planeAnchor as? ARPlaneAnchor, true)
        }
        
        // -------------------------------------------------------------------------------
        // 2. Collect more information about the environment by hit testing against
        //    the feature point cloud, but do not return the result yet.
        
        var featureHitTestPosition: SCNVector3?
        var highQualityFeatureHitTestResult = false
        
        let highQualityfeatureHitTestResults = sceneView.hitTestWithFeatures(position, coneOpeningAngleInDegrees: 18, minDistance: 0.2, maxDistance: 2.0)
        
        if !highQualityfeatureHitTestResults.isEmpty {
            let result = highQualityfeatureHitTestResults[0]
            featureHitTestPosition = result.position
            highQualityFeatureHitTestResult = true
        }
        
        // -------------------------------------------------------------------------------
        // 3. If desired or necessary (no good feature hit test result): Hit test
        //    against an infinite, horizontal plane (ignoring the real world).
        
        if (infinitePlane && dragOnInfinitePlanesEnabled) || !highQualityFeatureHitTestResult {
            
            let pointOnPlane = objectPos ?? SCNVector3Zero
            
            let pointOnInfinitePlane = sceneView.hitTestWithInfiniteHorizontalPlane(position, pointOnPlane)
            if pointOnInfinitePlane != nil {
                return (pointOnInfinitePlane, nil, true)
            }
        }
        
        // -------------------------------------------------------------------------------
        // 4. If available, return the result of the hit test against high quality
        //    features if the hit tests against infinite planes were skipped or no
        //    infinite plane was hit.
        
        if highQualityFeatureHitTestResult {
            return (featureHitTestPosition, nil, false)
        }
        
        // -------------------------------------------------------------------------------
        // 5. As a last resort, perform a second, unfiltered hit test against features.
        //    If there are no features in the scene, the result returned here will be nil.
        
        let unfilteredFeatureHitTestResults = sceneView.hitTestWithFeatures(position)
        if !unfilteredFeatureHitTestResults.isEmpty {
            let result = unfilteredFeatureHitTestResults[0]
            return (result.position, nil, false)
        }
        
        return (nil, nil, false)
    }
    
    // MARK: - Virtual Object Loading
    
    var trayTableObject: TrayTable?
    var isLoadingObject: Bool = false {
        didSet {
            DispatchQueue.main.async {
                // let em know
            }
        }
    }
    
    func loadTrayTableObject() {
        resetTrayTableObject()
        
        guard let screenCenter = self.screenCenter else { return }
        
        // Show progress indicator
        let spinner = UIActivityIndicatorView()
        spinner.center = screenCenter
        sceneView.addSubview(spinner)
        spinner.startAnimating()
        
        // Load the content asynchronously.
        DispatchQueue.global().async {
            self.isLoadingObject = true
            let object = BlankTrayTableObject()
            self.trayTableObject = object
            print("loading model")
            object.loadModel()
            
            DispatchQueue.main.async {
                // Immediately place the object in 3D space.
                if let lastFocusSquarePos = self.focusSquare?.lastPosition {
                    self.setNewTrayTableObjectPosition(lastFocusSquarePos)
                } else {
                    self.setNewTrayTableObjectPosition(SCNVector3Zero)
                }

                // Remove progress indicator
                spinner.removeFromSuperview()
                
                self.isLoadingObject = false
            }
        }
    }

    // MARK: - Tray Table Object Manipulation
    
    func setNewTrayTableObjectPosition(_ pos: SCNVector3) {
        
        guard let object = trayTableObject, let cameraTransform = session.currentFrame?.camera.transform else {
            return
        }
        print(object)
//        recentVirtualObjectDistances.removeAll()
        
        let cameraWorldPos = SCNVector3.positionFromTransform(cameraTransform)
        var cameraToPosition = pos - cameraWorldPos
        
        
        // Limit the distance of the object from the camera to a maximum of 10 meters.
        cameraToPosition.setMaximumLength(10)
        
        object.position = cameraWorldPos + cameraToPosition
        
        if object.parent == nil {
            sceneView.scene.rootNode.addChildNode(object)
        }
        
        // Correct y rotation of camera square
        if let camera = session.currentFrame?.camera {
            let tilt = abs(camera.eulerAngles.x)
            let threshold1: Float = Float.pi / 2 * 0.65
            let threshold2: Float = Float.pi / 2 * 0.75
            let yaw = atan2f(camera.transform.columns.0.x, camera.transform.columns.1.x)
            var angle: Float = 0
            
            switch tilt {
            case 0..<threshold1:
                angle = camera.eulerAngles.y
            case threshold1..<threshold2:
                let relativeInRange = abs((tilt - threshold1) / (threshold2 - threshold1))
                let normalizedY = normalize(camera.eulerAngles.y, forMinimalRotationTo: yaw)
                angle = normalizedY * (1 - relativeInRange) + yaw * relativeInRange
            default:
                angle = yaw
            }
            object.rotation = SCNVector4Make(0, 1, 0, angle)
        }
    }
    
    private func normalize(_ angle: Float, forMinimalRotationTo ref: Float) -> Float {
        // Normalize angle in steps of 90 degrees such that the rotation to the other angle is minimal
        var normalized = angle
        while abs(normalized - ref) > Float.pi / 4 {
            if angle > ref {
                normalized -= Float.pi / 2
            } else {
                normalized += Float.pi / 2
            }
        }
        return normalized
    }

    func resetTrayTableObject() {
        trayTableObject?.unloadModel()
        trayTableObject?.removeFromParentNode()
        trayTableObject = nil
    }
    
    // MARK: - Focus Square
    
    var focusSquare: FocusSquare?
    
    func setupFocusSquare() {
        focusSquare?.isHidden = true
        focusSquare?.removeFromParentNode()
        focusSquare = FocusSquare()
        sceneView.scene.rootNode.addChildNode(focusSquare!)
        
    }
    
    func updateFocusSquare() {
        guard let screenCenter = screenCenter else { return }
        
        if trayTableObject != nil && sceneView.isNode(trayTableObject!, insideFrustumOf: sceneView.pointOfView!) {
            focusSquare?.hide()
        } else {
            focusSquare?.unhide()
        }
        let (worldPos, planeAnchor, _) = worldPositionFromScreenPosition(screenCenter, objectPos: focusSquare?.position)
        if let worldPos = worldPos {
            focusSquare?.update(for: worldPos, planeAnchor: planeAnchor, camera: self.session.currentFrame?.camera)
        }
    }
    

}
