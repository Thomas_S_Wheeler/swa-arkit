//
//  Solitaire.swift
//  cards
//
//  Created by Thomas Wheeler on 8/7/17.
//  Copyright © 2017 Thomas Wheeler. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

class Solitaire : SCNNode {
    
    var board: SCNNode?
    
    private let width: Float = 1.0
    private let length: Float = 0.51
    private let height: Float = 0.005
    
    override init () {
        super.init()
        
        board = createBoard()
        self.addChildNode(board!)
    }
    
    func createBoard() -> SCNNode {
        let board = SCNNode()
        board.geometry = SCNBox(width: CGFloat(width), height: CGFloat(height), length: CGFloat(length), chamferRadius: 0)
        board.geometry?.materials.first?.diffuse.contents = UIColor.blue.withAlphaComponent(0.5)
        let boardMaterial = SCNMaterial()
        boardMaterial.diffuse.contents = UIColor.green
        board.geometry?.materials = [boardMaterial]
        board.scale = SCNVector3(0.3, 0.3, 0.3)
        return board
    }
    
    func setPosition(_ position: SCNVector3) {
        board?.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

