//
//  RectangleVisualization.swift
//  cards
//
//  Created by Thomas Wheeler on 8/7/17.
//  Copyright © 2017 Thomas Wheeler. All rights reserved.
//
import ARKit
import Vision
import Foundation

class RectangleVisualization {
    
    let sceneView: ARSCNView
    let overlayView = LineOverlayView()
    
    let session: ARSession
    
//    var node: SCNNode
//    var geometry: SCNGeometry
    
    var visionSequenceRequestHandler: VNSequenceRequestHandler = VNSequenceRequestHandler()
    lazy var rectRequest: VNRequest = {
        let request = VNDetectRectanglesRequest { (request, error) in
            guard let results = request.results as? [VNRectangleObservation]
                else { print("failed to detect rectangles"); return }
            
            //            print(results)
            DispatchQueue.main.async {
                let screenWidth = self.overlayView.frame.size.width
                let screenHeight = self.overlayView.frame.size.height
                for result in results {
                    
                    let scale = CGAffineTransform(scaleX: screenWidth, y: screenHeight)
                    let translate = CGAffineTransform(translationX: 0, y: screenHeight)
                    let scaleAgain = CGAffineTransform(scaleX: 1, y: -1)
                    
                    let topLeft = CGPoint(x: result.topLeft.y * screenWidth, y: (result.topLeft.x) * screenHeight)
                    let topRight = CGPoint(x: result.topRight.y * screenWidth, y: (result.topRight.x) * screenHeight)
                    let bottomLeft = CGPoint(x: result.bottomLeft.y * screenWidth, y: (result.bottomLeft.x) * screenHeight)
                    let bottomRight = CGPoint(x: result.bottomRight.y * screenWidth, y: (result.bottomRight.x) * screenHeight)
                    
                    self.overlayView.addLine(start: topLeft, end: topRight)
                    self.overlayView.addLine(start: topRight, end: bottomRight)
                    self.overlayView.addLine(start: bottomRight, end: bottomLeft)
                    self.overlayView.addLine(start: bottomLeft, end: topLeft)
                }
                
                self.overlayView.setNeedsDisplay()
            }
        }
        request.maximumObservations = 1
        request.minimumSize = 0.3
        return request
    }()
    init(sceneView: ARSCNView) {
        self.sceneView = sceneView
        overlayView.backgroundColor = UIColor.clear
        overlayView.frame = sceneView.frame
        sceneView.addSubview(overlayView)
        session = sceneView.session
//        geometry = SCNSphere(radius: 1)
//        geometry.materials.first?.diffuse.contents = UIColor.red
//        node = SCNNode(geometry: geometry)
//        sceneView.scene.rootNode.addChildNode(node)
    }
    
    deinit {
        overlayView.removeFromSuperview()
    }

    var counter = 0

    func render() {
        if counter % 3 == 0 { // every three frames
            DispatchQueue.global(qos: .userInteractive).async {
                do {
                    guard let pixelBuffer = self.session.currentFrame?.capturedImage
                        else { return }
                    try self.visionSequenceRequestHandler.perform([self.rectRequest], on: pixelBuffer)
                } catch {
                    print(error)
                }
            }
        }
        
        counter = (counter + 1) % 3
    }
}


class LineOverlayView: UIView {
    
    struct Line {
        var start: CGPoint
        var end: CGPoint
    }
    
    var lines = [Line]()

    func addLine(start: CGPoint, end: CGPoint) {
        lines.append(Line(start: start, end: end))
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        for line in lines {
            let path = UIBezierPath()
            path.move(to: line.start)
            path.addLine(to: line.end)
            path.close()
            UIColor.red.set()
            path.stroke()
            path.fill()
        }
        lines.removeAll()
    }
}

